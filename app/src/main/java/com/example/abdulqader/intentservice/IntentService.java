package com.example.abdulqader.intentservice;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;

public class IntentService  extends android.app.IntentService
{
    public IntentService() {
        super("my intent");
    }



    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        try {
            if(intent.getExtras().containsKey("testing_value")){
                int value=intent.getIntExtra("testing_value",0);
                Log.d("Intent service",String.valueOf(value));
            }
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
