package com.example.abdulqader.intentservice;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        for (int i=0;i<10;i++){

            Intent intent=new Intent(this,IntentService.class);
            intent.putExtra("testing_value",i);
            startService(intent);
        }

        Log.d("Intent service","done firing off service intents");


    }
}
